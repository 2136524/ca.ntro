package ca.ntro.core.task_graphs.handlers;

public interface RemovedFromGraphHandler {
	
	void onRemovedFromGraph();

}
