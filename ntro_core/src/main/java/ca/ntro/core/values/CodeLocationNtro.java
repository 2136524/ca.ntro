package ca.ntro.core.values;

public class CodeLocationNtro implements CodeLocation {
	
	private String filename;
	private int lineNumber;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	
	public CodeLocationNtro() {
	}

	public CodeLocationNtro(String filename, int lineNumber) {
		setFilename(filename);
		setLineNumber(lineNumber);
	}

	@Override
	public String filename() {
		return getFilename();
	}

	@Override
	public int lineNumber() {
		return getLineNumber();
	}

}
