package ca.ntro.core.values;

public interface CodeLocation {

	String filename();
	int lineNumber();

}
