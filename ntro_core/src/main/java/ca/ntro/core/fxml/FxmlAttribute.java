package ca.ntro.core.fxml;

public interface FxmlAttribute {
	
	/**
	 * The name of the attribute.
	 * Examples: "id" "styleClass" "text" "HBox.hgrow" "fx:id" "fx:controller" "xmlns:fx" "javafx.scene.control.ScrollPane" ...
	 * @return name
	 */
	public String getName();
	public void setName(String value);
	
	/**
	 * Returns the value of the attribute. Can be an empty string.
	 * @return value
	 */
	public String getValue();
	public void setValue(String value);
	
	
}
