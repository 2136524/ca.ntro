package ca.ntro.core.fxml;


import java.util.List;


/**
 * A FXML element. Cannot be a xml comment <!-- -->.
 *
 */
public interface FxmlElement {
	
	/**
	 * The element's/tag's name. Examples: "" "?xml" "?import" "VBox" "Button" "children" ...
	 * Can be an empty string if this is the root element returned by an implemented parser.
	 * @return name
	 */
	public String getName();
	public void setName(String value);
	
	public List<FxmlAttribute> getAttributes();
	public void setAttributes(List<FxmlAttribute> value);
	
	public List<FxmlElement> getChildren();
	public void setChildren(List<FxmlElement> value);
	
	
}
