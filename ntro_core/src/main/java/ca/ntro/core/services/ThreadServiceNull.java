package ca.ntro.core.services;

public class ThreadServiceNull implements ThreadService {

	@Override
	public void runOnMainThread(Runnable runnable) {
		runnable.run();
	}

	@Override
	public void runInWorkerThread(Runnable runnable) {
		runnable.run();
	}

	@Override
	public String currentThreadId() {
		// TODO Auto-generated method stub
		return null;
	}

}
