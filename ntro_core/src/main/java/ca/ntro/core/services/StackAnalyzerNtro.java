/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.core.services;

import java.util.HashMap;
import java.util.Map;

import ca.ntro.core.initialization.Ntro;

public abstract class StackAnalyzerNtro implements StackAnalyzer {
	
	private static final int DEFAULT_CALL_DISTANCE = 0;
	
	private Map<String, Integer> increments = new HashMap<>();
	

	@Override
	public void analyzeCall(Object calledClassOrObject) {
	}

	@Override
	public Class<?> callerClass() {
		return null;
	}

	public void incrementCallDistanceForCurrentThread() {
		addToCallDistanceForCurrentThread(+1);
	}

	public void addToCallDistanceForCurrentThread(int amount) {
		String threadId = Ntro.threads().currentThreadId();
		
		Integer currentIncrement = increments.get(threadId);
		
		if(currentIncrement != null) {
			
			increments.put(threadId, currentIncrement + amount);
			
		}else {

			increments.put(threadId, amount);
		}
	}

	public void decrementCallDistanceForCurrentThread() {
		addToCallDistanceForCurrentThread(-1);
	}
	
	protected int callDistance() {
		int callDistance = DEFAULT_CALL_DISTANCE;

		String threadId = Ntro.threads().currentThreadId();
		Integer currentIncrement = increments.get(threadId);
		
		if(currentIncrement != null) {
			callDistance += currentIncrement;
		}
		
		return callDistance;
	}

}
