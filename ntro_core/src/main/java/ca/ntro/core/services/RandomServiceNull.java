/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.core.services;

import java.util.List;

public class RandomServiceNull implements RandomService {

	@Override
	public boolean nextBoolean() {
		return false;
	}

	@Override
	public int nextInt() {
		return 0;
	}

	@Override
	public int nextInt(int bound) {
		return 0;
	}

	@Override
	public <V> V choice(List<V> list) {
		V value = null;
		if(list.size() > 0) {
			value = list.get(0);
		}
		return value;
	}

	@Override
	public <V> V choice(V[] array) {
		V value = null;
		if(array.length > 0) {
			value = array[0];
		}
		return value;
	}

	@Override
	public double nextDouble() {
		return 0;
	}

	@Override
	public double nextDouble(double bound) {
		return 0;
	}

	@Override
	public String nextId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String nextId(int size) {
		// TODO Auto-generated method stub
		return null;
	}

}

