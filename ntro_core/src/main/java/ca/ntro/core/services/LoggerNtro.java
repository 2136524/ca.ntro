/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.core.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ntro.core.initialization.Ntro;
import ca.ntro.core.values.CodeLocation;

public class LoggerNtro implements Logger {
	
	private List<String> tasks = new ArrayList<>();
	private Map<Integer, Integer> repeats = new HashMap<>();
	
	private PrintWriter logFileWriter;

	public LoggerNtro() {
		
		try {
			
			File storageDir = new File("_storage");
			if(!storageDir.exists()) {
				storageDir.mkdir();
			}

			logFileWriter = new PrintWriter(new File("_storage/exceptions.log"));

		} catch (FileNotFoundException e) {
			System.err.println("[FATAL] Cannot write log file _storage/exceptions.log");
			Ntro.exit(() -> {});
		}
	}
	
	public synchronized void flushTasks() {
		for(int i = 0; i < tasks.size(); i++) {
			String message = "[TASK] " + tasks.get(i);
			Integer repeatCount = repeats.get(i);
			if(repeatCount != null) {
				int totalCount = repeatCount + 1;
				message += " (" + (totalCount) + ")";
			}
			System.out.println(message);
		}
		
		tasks.clear();
		repeats.clear();
	}
	
	@Override
	public synchronized void taskExecutes(String taskId) {
		int repeatsIndex = tasks.size() - 1;

		if(!tasks.isEmpty()
				&& tasks.get(repeatsIndex).equals(taskId)) {
			
			Integer repeatCount = repeats.get(repeatsIndex);

			if(repeatCount == null) {

				repeatCount = 1;

			}else {

				repeatCount++;
			}

			repeats.put(repeatsIndex, repeatCount);
			

		}else {
			
			tasks.add(taskId);

		}
	}

	@Override
	public void run() {
		Ntro.time().runRepeatedly(1000, () -> {
			flushTasks();
		});
	}

	@Override
	public void info(String message) {
		flushTasks();

		System.out.println("[INFO] " + message);
	}

	@Override
	public void error(String message) {
		flushTasks();
		System.out.println("[ERROR] " + message);
	}

	@Override
	public void exception(Throwable t) {
		flushTasks();

		CodeLocation location = Ntro.stackAnalyzer().callingLocationOf(t);

		System.out.println("[EXCEPTION] (" + location.filename() + ":" + location.lineNumber() + ") " + t.getMessage());
	}

	@Override
	public void fatal(Throwable t) {
		flushTasks();
		
		CodeLocation location = Ntro.stackAnalyzer().callingLocationOf(t);
		
		String message = "(" + location.filename() + ":" + location.lineNumber() + ") ";
		
		Throwable cause = t;
		while(cause.getCause() != null) {
			cause = cause.getCause();
		}

		if(cause.getClass().getSimpleName().equals("RuntimeException")
			  || cause.getClass().getSimpleName().equals("Exception")
			  || cause.getClass().getSimpleName().equals("Error")) {

			message += cause.getMessage();
			
		}else {
			
			message += cause.getClass().getSimpleName() + " " + cause.getMessage();
		}
		
		
		fatal(message, cause);
	}
	
	protected void fatal(String message, Throwable t) {

		System.err.println("\n[FATAL] " + message);
		System.err.println("\n        (see _storage/exceptions.log for details)\n");
		
		t.printStackTrace(logFileWriter);
		logFileWriter.write("\n\n");
		logFileWriter.close();

		Ntro.exit(() -> {});
	}
	
	/*
	private Path traceFilePath = Path.emptyPath();                        // TODO: actual file path
	private Path exceptionFilePath = Path.emptyPath();                    // TODO: actual file path

	private FileOpener fileOpener = new FileOpenerNull();

	public void exception(Throwable e) {

		LocalTextFile exceptionFile = fileOpener.openLocalTextFile(exceptionFilePath);

		// TODO: better formatting
		exceptionFile.append(e.getMessage()).handleException(e2 -> {
			e2.printStackTrace();
		});
	}

	public void trace(String message) {

		LocalTextFile traceFile = fileOpener.openLocalTextFile(traceFilePath);

		traceFile.append(message).handleException(e -> {
			exception(e);
		});
	}
	*/
}
