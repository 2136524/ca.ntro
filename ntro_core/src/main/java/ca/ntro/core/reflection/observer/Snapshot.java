package ca.ntro.core.reflection.observer;

public interface Snapshot<V extends Observable> {

	String id();
	V currentValue();

}
