package ca.ntro.app.structures;

public interface ClassIdMapEntry<C,V> extends IdMapEntry<V> {
	
	Class<? extends C> entryClass();
	
	
	

}
