package ca.ntro.app.structures;

public interface IdMapEntry<V> {
	
	String entryId();
	V value();

}
