package ca.ntro.app.tasks.frontend;

import ca.ntro.app.tasks.MessageHandlerTaskDescriptorNtro;

public class FrontendMessageTaskDescriptorNtro<O>

       extends    MessageHandlerTaskDescriptorNtro<O>

       implements FrontendSimpleTaskDescriptor<O> {
	
	
	public FrontendMessageTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public FrontendMessageTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}
}
