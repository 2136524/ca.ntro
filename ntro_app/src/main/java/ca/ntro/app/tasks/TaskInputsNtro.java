/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.tasks;

import ca.ntro.app.NtroApp;
import ca.ntro.core.initialization.Ntro;
import ca.ntro.core.services.StackAnalyzerNtro;
import ca.ntro.core.task_graphs.task_graph.Task;
import ca.ntro.core.task_graphs.task_graph.TaskGraph;
import ca.ntro.core.values.ObjectMap;

public class TaskInputsNtro implements TaskInputs {
	
	private TaskGraph graph;
	private Task task;
	private ObjectMap inputs;

	public ObjectMap getInputs() {
		return inputs;
	}

	public void setInputs(ObjectMap inputs) {
		this.inputs = inputs;
	}

	public TaskGraph getGraph() {
		return graph;
	}

	public void setGraph(TaskGraph graph) {
		this.graph = graph;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public TaskInputsNtro(TaskGraph graph, Task task, ObjectMap inputs) {
		setGraph(graph);
		setTask(task);
		setInputs(inputs);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <O> O get(TaskDescriptor<O> task) {
		return (O) get(task.id());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object get(String id) {
		Object result = getInputs().get(id);
		
		if(result == null) {
			getGraph().write(Ntro.graphWriter());
			System.err.println("\n\n[FATAL] inputs.get(" + id + ") not found in task " + getTask().id());
			System.err.println("[FATAL] please add the required .waitsFor and/or correct the call to inputs.get()");
			System.err.println("\n");
			Ntro.exit(() -> {});
		}

		return result;
	}

}
