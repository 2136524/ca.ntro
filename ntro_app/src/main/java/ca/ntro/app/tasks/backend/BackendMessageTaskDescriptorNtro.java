package ca.ntro.app.tasks.backend;

import ca.ntro.app.tasks.MessageHandlerTaskDescriptorNtro;

public class BackendMessageTaskDescriptorNtro<O>

       extends    MessageHandlerTaskDescriptorNtro<O>

       implements BackendSimpleTaskDescriptor<O> {
	
	
	public BackendMessageTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public BackendMessageTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}
}
