package ca.ntro.app.tasks.frontend;

import ca.ntro.app.NtroApp;
import ca.ntro.app.models.Model;
import ca.ntro.app.tasks.ModelTaskDescriptorNtro;
import ca.ntro.core.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.core.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.core.task_graphs.task_graph.Task;
import ca.ntro.core.task_graphs.task_graph.TaskContainer;
import ca.ntro.core.task_graphs.task_graph_trace.TaskResultsMessageHandler;
import ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro;

public class FrontendModifiedTaskDescriptorNtro<O>

       extends    ModelTaskDescriptorNtro<O>

       implements FrontendSimpleTaskDescriptor<O> {
	
	public FrontendModifiedTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public FrontendModifiedTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}
	

	@Override
	public Task newTask(TaskContainer parent) {
		String modelId = getId();
		String taskId = id();

		Task modifiedHandler = parent.newTask(taskId,
										      SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
																	  .traceClass(TaskTraceNtro.class)
																	  .resultsClass(TaskResultsMessageHandler.class));

		NtroApp.messageService().addObserverTask((Class<? extends Model>) getEntryClass(), modelId, modifiedHandler.asSimpleTask());
		
		modifiedHandler.onRemovedFromGraph(() -> {
			
			NtroApp.messageService().removeObserverTask((Class<? extends Model>) getEntryClass(), modelId, modifiedHandler.asSimpleTask());

		});
		
		NtroApp.models().watch((Class<? extends Model>) getEntryClass(), modelId);
		
		return modifiedHandler;
	}

	@Override
	protected String taskLabel() {
		return "modified";
	}
}
