package ca.ntro.app.tasks.backend;

import ca.ntro.app.tasks.ModelTaskDescriptorNtro;
import ca.ntro.core.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.core.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.core.task_graphs.task_graph.Task;
import ca.ntro.core.task_graphs.task_graph.TaskContainer;
import ca.ntro.core.task_graphs.task_graph_trace.TaskResultsLock;
import ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro;

public class BackendModelTaskDescriptorNtro<O>

       extends    ModelTaskDescriptorNtro<O>

       implements BackendSimpleTaskDescriptor<O> {
	
	
	public BackendModelTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public BackendModelTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}

	@Override
	public Task newTask(TaskContainer parent) {
		String modelId = getId();
		String taskId = id();
		
		Task task = parent.newTask(taskId, SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
				                                            .traceClass(TaskTraceNtro.class)
				                                            .resultsClass(TaskResultsLock.class));

		return task;

	}

	@Override
	protected String taskLabel() {
		return "model";
	}
}
