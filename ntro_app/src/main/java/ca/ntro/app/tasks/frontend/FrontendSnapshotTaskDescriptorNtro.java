package ca.ntro.app.tasks.frontend;

import ca.ntro.app.NtroApp;
import ca.ntro.app.tasks.ModelTaskDescriptorNtro;
import ca.ntro.core.reflection.observer.Observable;
import ca.ntro.core.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.core.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.core.task_graphs.task_graph.Task;
import ca.ntro.core.task_graphs.task_graph.TaskContainer;
import ca.ntro.core.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro;

public class FrontendSnapshotTaskDescriptorNtro<O>

       extends    ModelTaskDescriptorNtro<O>

       implements FrontendSimpleTaskDescriptor<O> {
	
	
	public FrontendSnapshotTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public FrontendSnapshotTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}

	@Override
	public Task newTask(TaskContainer parent) {
		String modelId = getId();
		String taskId = id();
		
		Task snapshotTask = parent.newTask(taskId,
										   SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
															.traceClass(TaskTraceNtro.class)
														    .resultsClass(TaskResultsCondition.class));

		NtroApp.messageService().addSnapshotTask((Class<? extends Observable>) getEntryClass(), modelId, snapshotTask.asSimpleTask());
		
		snapshotTask.onRemovedFromGraph(() -> {

			NtroApp.messageService().removeSnapshotTask((Class<? extends Observable>) getEntryClass(), modelId, snapshotTask.asSimpleTask());

		});
		
		NtroApp.models().watch((Class<? extends Observable>) getEntryClass(), modelId);
		
		return snapshotTask;
	}

	@Override
	protected String taskLabel() {
		return "snapshot";
	}
}
