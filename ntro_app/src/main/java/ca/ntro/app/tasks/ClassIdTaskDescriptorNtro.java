package ca.ntro.app.tasks;

import ca.ntro.core.initialization.Ntro;
import ca.ntro.core.util.StringUtils;

public abstract class ClassIdTaskDescriptorNtro<O>

       extends        SimpleTaskDescriptorNtro<O> {
	
	
	private Class<?> entryClass;

	public Class<?> getEntryClass() {
		return entryClass;
	}

	public void setEntryClass(Class<?> entryClass) {
		this.entryClass = entryClass;
	}

	public ClassIdTaskDescriptorNtro(Class<?> entryClass) {
		super(null);
		setEntryClass(entryClass);
	}

	public ClassIdTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(id);
		setEntryClass(entryClass);
	}
	
	protected abstract String taskLabel();
	
	@Override
	public String id() {
		String modelId = getId();
		String id = taskLabel();
		id += "[" + Ntro.reflection().simpleName(getEntryClass());
		
		if(!StringUtils.isNullOrEmpty(modelId)) {

			id += "/" + modelId;
		}
		
		id += "]";

		return id;
	}
}
