package ca.ntro.app.tasks;


public abstract class ModelTaskDescriptorNtro<O>

       extends        ClassIdTaskDescriptorNtro<O> {
	
	
	public ModelTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public ModelTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}
}
