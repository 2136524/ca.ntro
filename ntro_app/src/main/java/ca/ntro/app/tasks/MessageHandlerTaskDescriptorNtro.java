package ca.ntro.app.tasks;

import ca.ntro.app.NtroApp;
import ca.ntro.app.messages.Message;
import ca.ntro.core.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.core.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.core.task_graphs.task_graph.Task;
import ca.ntro.core.task_graphs.task_graph.TaskContainer;
import ca.ntro.core.task_graphs.task_graph_trace.TaskResultsMessageHandler;
import ca.ntro.core.task_graphs.task_graph_trace.TaskTraceNtro;

public class MessageHandlerTaskDescriptorNtro<O>

       extends    ClassIdTaskDescriptorNtro<O> {

	
	public MessageHandlerTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public MessageHandlerTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}

	@Override
	public Task newTask(TaskContainer parent) {
		String channelId = getId();
		String taskId = id();
		
		Task messageHandler = parent.newTask(taskId, SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
				                                                      .traceClass(TaskTraceNtro.class)
				                                                      .resultsClass(TaskResultsMessageHandler.class));

		NtroApp.messageService().addMessageHandler((Class<? extends Message>) getEntryClass(), channelId, messageHandler.asSimpleTask());
		
		messageHandler.onRemovedFromGraph(() -> {
			NtroApp.messageService().removeMessageHandler((Class<? extends Message>) getEntryClass(), channelId, messageHandler.asSimpleTask());
		});

		return messageHandler;

	}

	@Override
	protected String taskLabel() {
		return "message";
	}
}
