/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.services;

import ca.ntro.app.messages.DeliveryMode;
import ca.ntro.app.messages.Message;
import ca.ntro.app.messages.MessageNtro;
import ca.ntro.app.messages.MessageServer;
import ca.ntro.app.messages.MessageServerNull;
import ca.ntro.app.structures.ValuesByClassId;
import ca.ntro.core.initialization.Ntro;
import ca.ntro.core.reflection.observer.Modified;
import ca.ntro.core.reflection.observer.Observable;
import ca.ntro.core.reflection.observer.Observation;
import ca.ntro.core.reflection.observer.Snapshot;
import ca.ntro.core.task_graphs.task_graph.SimpleTask;

public class MessageServiceNtro implements MessageService {
	
	private ValuesByClassId<Message, SimpleTask> messageHandlers = new ValuesByClassId<>();
	private ValuesByClassId<Observable, SimpleTask> observationHandlers = new ValuesByClassId<>();
	private ValuesByClassId<Observable, SimpleTask> snapshotHandlers = new ValuesByClassId<>();

	private MessageServer messageServer = new MessageServerNull();
	private DeliveryMode deliveryMode = DeliveryMode.LOCAL;

	@Override
	public <MSG extends MessageNtro> MSG newMessage(Class<MSG> messageClass) {
		return newMessage(messageClass, null);
	}

	@Override
	public <MSG extends MessageNtro> MSG newMessage(Class<MSG> messageClass, String channelId) {
		MSG message = Ntro.factory().newInstance(messageClass);

		message.registerMessageService(this);
		message.setChannelId(channelId);

		return message;
	}

	@Override
	public <MSG extends Message> void addMessageHandler(Class<MSG> messageClass, SimpleTask messageHandlerTask) {
		addMessageHandler(messageClass, null, messageHandlerTask);
	}

	@Override
	public <MSG extends Message> void addMessageHandler(Class<MSG> messageClass, String channelId, SimpleTask messageHandlerTask) {
		messageHandlers.add(messageClass, channelId, messageHandlerTask);
	}

	@Override
	public <MSG extends Message> void removeMessageHandler(Class<MSG> messageClass, SimpleTask messageHandlerTask) {
		removeMessageHandler(messageClass, null, messageHandlerTask);
	}

	@Override
	public <MSG extends Message> void removeMessageHandler(Class<MSG> messageClass, String channelId, SimpleTask messageHandlerTask) {
		messageHandlers.remove(messageClass, channelId, messageHandlerTask);
	}

	@Override
	public void addObserverTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		addObserverTask(observableClass, null, observationHandlerTask);
	}

	@Override
	public void addObserverTask(Class<? extends Observable> observableClass, String observableId, SimpleTask observationHandlerTask) {
		observationHandlers.add(observableClass, observableId, observationHandlerTask);
	}

	@Override
	public void removeObserverTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		addObserverTask(observableClass, null, observationHandlerTask);
	}

	@Override
	public void removeObserverTask(Class<? extends Observable> observableClass, String observableId, SimpleTask observationHandlerTask) {
		observationHandlers.remove(observableClass, observableId, observationHandlerTask);
	}

	@Override
	public void addSnapshotTask(Class<? extends Observable> observableClass, String observableId, SimpleTask observationHandlerTask) {
		snapshotHandlers.add(observableClass, observableId, observationHandlerTask);
	}

	@Override
	public void addSnapshotTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		addSnapshotTask(observableClass, null, observationHandlerTask);
	}

	@Override
	public void removeSnapshotTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		addSnapshotTask(observableClass, null, observationHandlerTask);
	}

	@Override
	public void removeSnapshotTask(Class<? extends Observable> observableClass, String observableId, SimpleTask observationHandlerTask) {
		snapshotHandlers.remove(observableClass, observableId, observationHandlerTask);
	}


	@Override
	public void sendMessageToServer(Message message) {
		messageServer.sendMessageToServer(message);
	}

	@Override
	public void broadcastMessageToOtherClients(Message message) {
		switch(deliveryMode) {
			case LOCAL:
			default:
				deliverMessageLocally((MessageNtro) message);
				break;

			case CLIENT_MODE:
			case SERVER_MODE:
				messageServer.broadcastMessageToOtherClients(message);
				break;
		}
	}

	@Override
	public void receiveMessageFromServer(Message message) {
		messageHandlers.values(message.getClass(), message.getChannelId()).forEach(handler -> {

			addMessageToMessageHandlerTask(handler, message);

		});
	}

	protected void addMessageToMessageHandlerTask(SimpleTask handler, Message message) {
		handler.addResult(message);
	}

	@Override
	public <O extends Observable> void receiveObservationFromServer(Class<O> observableClass, Observation<?> observation) {
		receiveObservationFromServer(observableClass, null, observation);
	}

	@Override
	public <O extends Observable> void receiveObservationFromServer(Class<O> observableClass, String observableId, Observation<?> observation) {
		
		observationHandlers.values(observableClass, observableId).forEach(handler -> {

			addObservationToObservationHandlerTask(handler, observation);
		});
		
		snapshotHandlers.values(observableClass, observableId).forEach(handler -> {

			addSnapshotToSnapshotHandlerTask(handler, (Snapshot) observation);
		});

		snapshotHandlers.clear(observableClass, observableId);
	}

	protected void addObservationToObservationHandlerTask(SimpleTask handler, Observation<?> observation) {
		handler.addResult((Modified<?>) observation);
	}

	protected void addSnapshotToSnapshotHandlerTask(SimpleTask handler, Snapshot<?> snapshot) {
		handler.addResult(snapshot);
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, String observationId, Observation<O> observation) {
		messageServer.pushObservationToClients(observableClass, observationId, observation);
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observationClass, Observation<O> observation) {
		pushObservationToClients(observationClass, null, observation);
	}

	public void deliverMessage(MessageNtro message) {
		switch(deliveryMode) {
			case LOCAL:
			case SERVER_MODE:
			default:
				deliverMessageLocally(message);
				break;

			case CLIENT_MODE:
				sendMessageToServer(message);
				break;
		}
	}

	private void deliverMessageLocally(MessageNtro message) {
		// XXX: copy message to simulate that the
		//      message is sent through a channel
		MessageNtro copyOfMessage = (MessageNtro) Ntro.reflection().clone(message);
		copyOfMessage.registerMessageService(this);
		receiveMessageFromServer(copyOfMessage);
	}

	@Override
	public void registerMessageServer(MessageServer messageServer, DeliveryMode deliveryMode) {
		this.messageServer = messageServer;
		this.deliveryMode = deliveryMode;
		
		this.messageServer.onMessageFromServer(message -> {
			receiveMessageFromServer(message);
		});
		
		this.messageServer.onObservationFromServer((observableClass, observableId, observation) -> {
			receiveObservationFromServer((Class<? extends Observable>) observableClass, observableId, observation);
		});
	}


}
