/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.services;

import ca.ntro.app.NtroApp;
import ca.ntro.app.models.Model;
import ca.ntro.app.models.WatchJson;
import ca.ntro.app.models.WriteObjectGraph;
import ca.ntro.app.structures.ClassIdMap;
import ca.ntro.core.initialization.Ntro;
import ca.ntro.core.json.JsonObject;
import ca.ntro.core.path.Path;
import ca.ntro.core.reflection.object_graph.ObjectGraph;
import ca.ntro.core.reflection.observer.Observable;
import ca.ntro.core.reflection.observer.ObservationNtro;
import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.StreamNtro;
import ca.ntro.core.stream.Visitor;
import ca.ntro.core.util.StringUtils;

public class ModelStoreDefault implements ModelStore {
	
	private boolean areDiskOperationsEnabled = true;
	
	private ClassIdMap<Object, Object> previousModels = new ClassIdMap<>();
	private ClassIdMap<Object, Object> currentModels = new ClassIdMap<>();

	@Override
	public <M extends Model> M load(Class<?> modelClass, String modelId) {

		M model = (M) currentModels.get(modelClass, modelId);
		
		if(model == null) {

			model = (M) loadFromFile(modelClass, modelId);

		}
		
		if(model == null) {
			
			model = (M) Ntro.factory().newInstance(modelClass);

		}
		
		try {

			previousModels.put(modelClass, modelId, Ntro.reflection().clone(model));

		}catch(Throwable t) {

			System.err.println("\n\n[FATAL] cannnot clone model " + modelClass.getSimpleName());
			System.err.println(t.getMessage());
			System.err.println("\n\n");
			Ntro.exit(() -> {});
		}

		currentModels.put(modelClass, modelId, model);
		
		return model;
	}
	
	@Override
	public <M extends Model> M load(Class<?> modelClass) {
		return load(modelClass, null);
	}

	private synchronized <M extends Model> M loadFromJsonString(Class<?> modelClass, 
																String modelId, 
			                                                    String jsonString) {
		M model = null;

		JsonObject jsonObject = Ntro.json().fromJsonString(jsonString);
			
		try {

			model = (M) Ntro.reflection().graphFromJsonObject(jsonObject).buildObject();

		}catch(Throwable t) {

			System.out.println("\n\n[WARNING] cannnot load model " + filePathFromClass(modelClass, modelId).toString());
			System.out.println(t.getMessage().replace("[FATAL]", "[WARNING]"));
			System.out.println("[WARNING] using empty model for " + modelClass.getSimpleName() + "\n\n");
			
			model = (M) Ntro.factory().newInstance(modelClass);
		}

		return model;
	}


	private synchronized <M extends Model> M loadFromFile(Class<?> modelClass, String modelId) {
		M model = null;

		Path filePath = filePathFromClass(modelClass, modelId);

		if(Ntro.storage().ifFileExists(filePath)) {

			String jsonString = Ntro.storage().readTextFile(filePath);
			
			model = loadFromJsonString(modelClass, modelId, jsonString);

		}else {

			System.err.println("\n\n[WARNING] cannnot load model " + filePath);
			System.err.println("[WARNING] using empty model for " + modelClass.getSimpleName() + "\n\n");
			
			model = (M) Ntro.factory().newInstance(modelClass);

		}

		return model;
	}

	private Path filePathFromClass(Class<?> modelClass, String modelId) {
		Path path = null;
		
		if(StringUtils.isNullOrEmpty(modelId)) {
			
			path = Path.fromRawPath("/models/" + Ntro.reflection().simpleName(modelClass) + ".json");
			
		}else {

			path = Path.fromRawPath("/models/" + Ntro.reflection().simpleName(modelClass) + "/" + modelId + ".json");

		}

		return path;
	}

	@Override
	public void save(Class<?> modelClass, Object model) {
		save(modelClass, null, model);
	}

	@Override
	public void save(Class<?> modelClass, String modelId, Object model) {

		Object previousModel = previousModels.get(modelClass, modelId);
		
		if(model instanceof WatchJson
				&& areDiskOperationsEnabled) {

			writeModelFile(modelClass, modelId, model);

		}else {

			pushObservation(modelClass, modelId, previousModel, model);

		}
	}

	private synchronized void writeModelFileBlocking(Path filePath, Object model) {
		ObjectGraph graph = Ntro.reflection().graphFromObject(model);
		
		JsonObject jsonObject = graph.buildJsonObject();
		String jsonString = jsonObject.toJsonString(true);

		Ntro.storage().writeTextFileBlocking(filePath, jsonString);

	}

	private synchronized void writeModelFile(Path filePath, Object model) {
		ObjectGraph graph = Ntro.reflection().graphFromObject(model);
		
		JsonObject jsonObject = graph.buildJsonObject();
		String jsonString = jsonObject.toJsonString(true);

		Ntro.storage().writeTextFile(filePath, jsonString);

	}

	private synchronized void writeModelFile(Class<?> modelClass, String modelId, Object model) {
		writeModelFile(filePathFromClass(model.getClass(), modelId), model);
	}

	private synchronized void writeModelFileBlocking(Class<?> modelClass, String modelId, Object model) {
		writeModelFileBlocking(filePathFromClass(modelClass, modelId), model);
	}

	@Override
	public void writeModelFiles() {
		currentModels.entries().forEach(entry -> {
			writeModelFileBlocking(entry.entryClass(), entry.entryId(), entry.value());
		});
	}


	@Override
	public void writeGraphs() {
		currentModels.entries().forEach(entry -> {
			Object model = entry.value();

			if(model instanceof WriteObjectGraph) {
				writeModelGraph(entry.entryClass(), entry.entryId(), model);
			}
		});
	}

	private void writeModelGraph(Class<?> modelClass, String modelId, Object model) {
		ObjectGraph graph = Ntro.reflection().graphFromObject(model, graphName(modelClass, modelId));
		graph.write(Ntro.graphWriter());
	}
	
	private String graphName(Class<?> modelClass, String modelId) {

		String graphName = Ntro.reflection().simpleName(modelClass);
		
		if(!StringUtils.isNullOrEmpty(modelId)) {

			graphName += "_" + modelId;

		}
		
		return graphName;
	}
	
	

	@Override
	public void watch(Class<? extends Observable> modelClass, String modelId) {

		Path filePath = filePathFromClass(modelClass, modelId);

		Object model = load(modelClass, modelId);

		createModelFileIfNeeded(filePath, modelClass, model);
		
		pushFirstObservation(modelClass, modelId);

		if(Ntro.reflection().ifClassImplements(modelClass, WatchJson.class)) {
			Ntro.storage().watchFile(filePath, jsonString -> {
				onModelFileChanged(modelClass, modelId, jsonString);
			});
		}
	}

	@Override
	public void watch(Class<? extends Observable> modelClass) {
		watch(modelClass, null);
	}

	private void createModelFileIfNeeded(Path filePath, Class<?> modelClass, Object model) {
		if(!Ntro.storage().ifFileExists(filePath)) {
			writeModelFile(filePath, model);
		}
	}

	private void onModelFileChanged(Class<? extends Observable> modelClass, String modelId, String jsonString) {
		Object previousModel = currentModels.get(modelClass, modelId);
		Object currentModel = loadFromJsonString(modelClass, modelId, jsonString);
		
		currentModels.put(modelClass, modelId, currentModel);

		pushObservation(modelClass, modelId, previousModel, currentModel);
	}
	
	private void pushFirstObservation(Class<? extends Observable> modelClass, String modelId) {
		Object previousModel = Ntro.factory().newInstance(modelClass);
		Object currentModel = load(modelClass, modelId);

		pushObservation(modelClass, modelId, previousModel, currentModel);
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void pushObservation(Class<?> modelClass, 
			                     String modelId, 
			                     Object previousModel, 
			                     Object currentModel) {

		if(previousModel != null) {

			ObservationNtro observation = new ObservationNtro<>();

			observation.setId(modelId);
			observation.setPreviousValue((Observable) previousModel);
			observation.setCurrentValue((Observable) currentModel);

			//Revisions revisions = Ntro.reflection().revisionsFromTo(initialModel, currentModel);

			NtroApp.messageService().receiveObservationFromServer((Class<? extends Observable>) modelClass, modelId, observation);
			NtroApp.messageService().pushObservationToClients((Class<? extends Observable>) modelClass, modelId, observation);
		}
	}


	@Override
	public Stream<Model> modelStream() {
		return new StreamNtro<Model>() {
			@Override
			public void forEach_(Visitor<Model> visitor) throws Throwable {
				currentModels.values().forEach(model -> {
					visitor.visit((Model) model);
				});
			}
		};
	}

	@Override
	public void suspendDiskOperations() {
		this.areDiskOperationsEnabled = false;
	}

	@Override
	public void resumeDiskOperations() {
		this.areDiskOperationsEnabled = true;
	}

	@Override
	public <M extends Model> void delete(Class<M> modelClass, String modelId) {
		deleteModelFile(modelClass, modelId);
	}

	@Override
	public <M extends Model> void delete(Class<M> modelClass) {
		deleteModelFile(modelClass);
	}

	private synchronized void deleteModelFile(Class<?> modelClass, String modelId) {
		Path filePath = filePathFromClass(modelClass, modelId);
		if(Ntro.storage().ifFileExists(filePath)) {
			Ntro.storage().deleteTextFile(filePath);
		}
	}

	private synchronized void deleteModelFile(Class<?> modelClass) {
		deleteModelFile(modelClass, null);
	}

}
