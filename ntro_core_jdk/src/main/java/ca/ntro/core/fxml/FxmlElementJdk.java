package ca.ntro.core.fxml;

import java.util.List;
import java.util.ArrayList;

public class FxmlElementJdk implements FxmlElement {
	
	private String name = "";
	private List<FxmlAttribute> attributes = null;
	private List<FxmlElement> children = null;

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String value) {
		this.name = value;
		
	}

	@Override
	public List<FxmlAttribute> getAttributes() {
		return this.attributes;
	}

	@Override
	public void setAttributes(List<FxmlAttribute> value) {
		this.attributes = value;
	}

	@Override
	public List<FxmlElement> getChildren() {
		return this.children;
	}

	@Override
	public void setChildren(List<FxmlElement> value) {
		this.children = value;
	}

	public FxmlElementJdk() {
		this.attributes = new ArrayList<>();
		this.children = new ArrayList<>();
	}
	
}
