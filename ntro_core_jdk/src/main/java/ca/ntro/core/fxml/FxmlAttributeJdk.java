package ca.ntro.core.fxml;

public class FxmlAttributeJdk implements FxmlAttribute {
	
	private String name = "";
	private String value = "";

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String value) {
		this.name = value;
	}

	@Override
	public String getValue() {
		return this.value;
	}

	@Override
	public void setValue(String value) {
		this.value = value;
	}

}
