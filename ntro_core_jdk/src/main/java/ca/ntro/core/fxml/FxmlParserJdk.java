package ca.ntro.core.fxml;


import java.util.List;
import java.util.ArrayList;
import java.util.Stack;


public class FxmlParserJdk implements FxmlParser {
	
	
	private String text = "";
	
	//Current reading position. This is the index of the next char to be returned by this.readChar().
	private int position = 0;
	
	
	//Reading opperations are easier when we can save our current reading position (of the text) and restore it later.
	private Stack<Integer> positionStack;
	
	private void save() {
		this.positionStack.add(this.position);
	}
	private void unsave() {
		this.position = this.positionStack.pop();
	}
	private void popsave() {
		this.positionStack.pop();
	}
	
	
	private boolean hasNextChar() {
		return 0 <= this.position && this.position < this.text.length();
	}
	
	/**
	 * Reads and consumes the next caracter
	 * @return the next char.
	 */
	private char readChar() {
		if (this.hasNextChar()) {
			this.position++;
			return this.text.charAt(this.position - 1);
		}
		return (char)-1;
	}
	
	
	/**
	 * Reads, but does not consumes the next char
	 * @return the next char
	 */
	private char peekChar() {
		if (this.hasNextChar()) {
			return this.text.charAt(this.position);
		}
		return (char)-1;
	}
	
	
	
	
	public FxmlParserJdk() {
		this.positionStack = new Stack();
		
		
	}
	
	
	

	@Override
	public FxmlElement parse(String text) {
		//cleaning
		this.position = 0;
		this.positionStack.clear();
		this.text = text;
		
		
		//When we begin reading the text, we are in the context of <tag></tag> reading. we either skip spaces, tabs, newlines or comments until we encounter a tag opening
		
		
		return null;
	}

	
	
	/**
	 * Skip every space, tab, newline and comments until the beginning of the comming tag/element.
	 * When this method ends, the next char to be read is '<', or it is the end of the file.
	 */
	private void skipUntilNextElement() {
		//until the next element, or the end of the file
		while (this.hasNextChar()) {
			char peek = this.peekChar();
			
			//we check if it's a whitespace
			if (peek == ' ' || peek == '\t' || peek == '\n') {
				//we consume that character
				this.readChar();
			}
			else if (peek == '<') {
				//we verify if this is a comment or the opening of an element/tag.
				
				this.save(); //if this is a comment or not, we come back here anyway
				
				boolean isComment = false; //becomes trus if this ends up to be a comment.
				
				this.readChar();
				if (this.hasNextChar()) {
					if (this.peekChar() == '!') {
						this.readChar();
						if (this.hasNextChar()) {
							if (this.peekChar() == '-') {
								this.readChar();
								if (this.hasNextChar()) {
									if (this.peekChar() == '-') {
										isComment = true;
									}
								}
							}
						}
					}
				}
				this.unsave();
				
				
				//if isComment == false, then we landed on an element, and we are done skipping characters
				if (!isComment) {
					break;
				}
				//we landed on a comment. we skip it until the end of the comment "-->"
				else {
//					this.readChar();
//					this.readChar();
//					this.readChar();
//					this.readChar();
					this.position += 4;

					char last0 = '\0';
					char last1 = '\0';
					char last2 = '\0';
					
					//until the end of the comment or the end of the file
					while (this.hasNextChar()) {
						last2 = last1;
						last1 = last0;
						last0 = this.readChar();
						
						//we check if we read "-->"
						if (last2 == '-' && last1 == '-' && last0 == '>') {
							//we are done skipping the comment
							break;
						}
					}
					
				}
				
			}
			//we are reading(peeking) garbage?
			else {
				//whatever this is, we are done skipping until the next element
				break;
			}
			
			
		}
		
		
	}

	
	/**
	 * 
	 * @return the next element. If null, the parsing is completed.
	 */
	private FxmlElement readNextElement() {
		//we skip every useless characters
		this.skipUntilNextElement();
		//the next char is either '<' or it is the end of the file.
		if (this.hasNextChar()) {
			
			
			
		}
		
		return null;
	}
	
	
	/**
	 * 
	 * @return the next attribute. If null, we are done reading the opening tag of the element
	 */
	private FxmlAttribute readNextAttribute() {
		
		return null;
	}
	
	
	
	
	
	
	
	
	


}






























