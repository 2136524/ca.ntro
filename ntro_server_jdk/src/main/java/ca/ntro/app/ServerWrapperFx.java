package ca.ntro.app;

import java.io.IOException;

import ca.ntro.app.backend.BackendRegistrarNtro;
import ca.ntro.app.messages.MessageRegistrarNtro;
import ca.ntro.app.models.ModelRegistrarNtro;
import ca.ntro.core.initialization.Ntro;
import javafx.application.Application;
import javafx.stage.Stage;

public class ServerWrapperFx extends Application {
	
	public static Class<? extends NtroServerJdk> serverClass;
	
	private NtroServerJdk serverSpec;
	private WebSocketServerNtro webSocketServer;

    private MessageRegistrarNtro   messageRegistrar  = new MessageRegistrarNtro();
    private ModelRegistrarNtro     modelRegistrar    = new ModelRegistrarNtro();
    private BackendRegistrarNtro   backendRegistrar  = new BackendRegistrarNtro(modelRegistrar, messageRegistrar);
    private ServerRegistrarJdkImpl serverRegistrar   = new ServerRegistrarJdkImpl();

	public NtroServerJdk getServerSpec() {
		return serverSpec;
	}

	public void setServerSpec(NtroServerJdk serverSpec) {
		this.serverSpec = serverSpec;
	}

	public WebSocketServerNtro getWebSocketServer() {
		return webSocketServer;
	}

	public void setWebSocketServer(WebSocketServerNtro webSocketServer) {
		this.webSocketServer = webSocketServer;
	}

	public ServerWrapperFx() {
	}

	public ServerWrapperFx(NtroServerJdk serverSpec) {
		setServerSpec(serverSpec);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {

    	Ntro.logger().run();
		
		serverSpec = Ntro.factory().newInstance(serverClass);
		
		serverSpec.registerMessages(messageRegistrar);
		serverSpec.registerModels(modelRegistrar);
		serverSpec.registerBackend(backendRegistrar);
		serverSpec.registerServer(serverRegistrar);

        System.out.println("\n\n\n");
        Ntro.logger().info("Ntro version 0.1");
        Ntro.logger().info("Locale: '" + NtroApp.currentLocale() + "'");
        
		backendRegistrar.prepareToExecuteTasks();
		backendRegistrar.executeTasks();

    	webSocketServer = new WebSocketServerNtro(serverRegistrar.getServerName(), serverRegistrar.getPort());
    	webSocketServer.start();

        new Thread() {
            
            @Override
            public void run() {

				Ntro.logger().info("App running. Press Enter here to close App");

                try {

                    System.in.read();
                    Ntro.exit(() -> {
                    	onExit();
                    });

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }.start();

	}

	public void onExit() {

    	Ntro.logger().info("Writing JSON files");
        NtroApp.models().writeModelFiles();

    	Ntro.logger().info("Generating graphs");
        NtroApp.models().writeGraphs();

        if(!backendRegistrar.isRemoteBackend()) {
			backendRegistrar.writeGraph();
        }
	}

}
