package ca.ntro.test;


import ca.ntro.app.frontend.FrontendFx;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.frontend.events.EventRegistrar;
import ca.ntro.app.services.Window;
import ca.ntro.app.tasks.frontend.FrontendTasks;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

public class TestFxFrontend implements FrontendFx {
    
    @Override
    public void createTasks(FrontendTasks tasks) {

        tasks.task("showWindow")
        
             .waitsFor(window())
             
             .thenExecutes(inputs -> {

                 Window window = inputs.get(window());

                 window.resize(600, 400);

                 window.show();

             });
    }

    @Override
    public void registerEvents(EventRegistrar registrar) {
        
    }

    @Override
    public void registerViews(ViewRegistrarFx registrar) {
        
    }

    @Override
    public void execute() {
        
    }

}
