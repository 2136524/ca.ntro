package ca.ntro.app.fx.controls;

public interface SizeListener {
	
	void newSize(double oldWidth, double oldHeight, double newWidth, double newHeight);

}
