/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.fx.controls;

import javafx.scene.input.MouseEvent;

public class World2dMouseEventFx {
	
	private MouseEvent rawMouseEvent;
	private World2dCanvasFx world2dCanvasFx;
	private double worldX;
	private double worldY;

	@SuppressWarnings("rawtypes")
	public World2dMouseEventFx(MouseEvent rawMouseEvent, 
			                   World2dCanvasFx world2dCanvasFx, 
			                   double worldX, 
			                   double worldY) {
		
		this.rawMouseEvent = rawMouseEvent;
		this.world2dCanvasFx = world2dCanvasFx;
		this.worldX = worldX;
		this.worldY = worldY;
	}

	public MouseEvent rawMouseEvent() {
		return rawMouseEvent;
	}

	public double canvasX() {
		return rawMouseEvent.getX();
	}

	public double canvasY() {
		return rawMouseEvent.getY();
	}

	public double worldX() {
		return worldX;
	}

	public double worldY() {
		return worldY;
	}

	public double worldWidth() {
		return world2dCanvasFx.worldWidth();
	}

	public double worldHeight() {
		return world2dCanvasFx.worldHeight();
	}

	public double viewportTopLeftX() {
		return world2dCanvasFx.viewportTopLeftX();
	}

	public double viewportTopLeftY() {
		return world2dCanvasFx.viewportTopLeftY();
	}

	public double viewportWidth() {
		return world2dCanvasFx.viewportWidth();
	}

	public double viewportHeight() {
		return world2dCanvasFx.viewportHeight();
	}

	public double widthOnScreen(double widthInWorld) {
		return ((World2dDrawingContext) world2dCanvasFx).widthOnScreen(widthInWorld);

	}

	public double heightOnScreen(double heightInWorld) {
		return ((World2dDrawingContext) world2dCanvasFx).heightOnScreen(heightInWorld);
	}

	public double widthInWorld(double widthOnScreen) {
		return ((World2dDrawingContext) world2dCanvasFx).widthInWorld(widthOnScreen);
	}

	public double heightInWorld(double heightOnScreen) {
		return ((World2dDrawingContext) world2dCanvasFx).heightInWorld(heightOnScreen);
	}
}
