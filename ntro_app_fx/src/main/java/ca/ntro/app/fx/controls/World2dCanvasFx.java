package ca.ntro.app.fx.controls;

import ca.ntro.app.frontend.events.EventNtro;
import ca.ntro.app.fx.world2d.World2dFx;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.input.MouseEvent;
import javafx.scene.transform.Affine;

public class World2dCanvasFx extends ResizableCanvasFx implements World2dDrawingContext {
	
	private ResizeMode resizeMode = ResizeMode.SCALE_DRAWING;
	private PositionMode positionMode = PositionMode.MIDDLE_CENTER;
	private AspectMode aspectMode = AspectMode.PRESERVE_ASPECT_RATIO;

    private double viewportTopLeftX = 0;
    private double viewportTopLeftY = 0;
    private double viewportWidth = 640;
    private double viewportHeight = 360;
    
    private double worldWidth = 640;
    private double worldHeight = 360;
    
    private Affine drawOnWorldTransform = new Affine();
    
    private NeedsRedrawListener needsRedrawListener = null;
    
	@Override
	protected void initialize() {
		onNewSize((oldWidth, oldHeight, newWidth, newHeight) -> {
			
			
			if(resizeMode == ResizeMode.RESIZE_VIEWPORT) {
					
				double scaleX = canvasWidth() / worldWidth;
				double scaleY = canvasHeight() / worldHeight;
				
				if(aspectMode == AspectMode.PRESERVE_ASPECT_RATIO) {
					scaleX = Math.min(scaleX, scaleY);
					scaleY = scaleX;
				}
				
				viewportWidth *= scaleX;
				viewportHeight *= scaleY;
			}

			updateDrawOnWorldTransform();
			
			if(needsRedrawListener != null) {
				needsRedrawListener.needsRedraw();
			}
					
		});
	}
	
	public void onNeedsRedraw(NeedsRedrawListener needsRedrawListener) {
		this.needsRedrawListener = needsRedrawListener;
	}

	private void updateDrawOnWorldTransform() {
        double scaleX = 1.0;
        double scaleY = 1.0;
        double translateX = 0;
        double translateY = 0;
        
        scaleX = canvasWidth() / viewportWidth;
        scaleY = canvasHeight() / viewportHeight;
        
        if(aspectMode == AspectMode.PRESERVE_ASPECT_RATIO) {
			scaleX = Math.min(scaleX, scaleY);
			scaleY = scaleX;
        }
        
        if(positionMode == PositionMode.TOP_LEFT) {

			translateX = 0;
			translateY = 0;
        	
        }else if(positionMode == PositionMode.MIDDLE_CENTER) {

			translateX = (canvasWidth() - viewportWidth * scaleX) / 2;
			translateY = (canvasHeight() - viewportHeight * scaleY) / 2;
        }
        
        drawOnWorldTransform.setMxx(scaleX);
        drawOnWorldTransform.setMyy(scaleY);
        drawOnWorldTransform.setTx(translateX);
        drawOnWorldTransform.setTy(translateY);
	}

    public void drawWorld2d(World2dFx world2d) {
    	this.worldWidth = world2d.getWidth();
    	this.worldHeight = world2d.getHeight();
    	
        getGc().save();

        getGc().beginPath();

        getGc().setTransform(drawOnWorldTransform);

        getGc().rect(viewportTopLeftX, 
                     viewportTopLeftY, 
                     viewportWidth, 
                     viewportHeight);
        
        getGc().clip();
        
        world2d.draw(this);

        getGc().restore();
    }

	@Override
	public double viewportTopLeftX() {
		return viewportTopLeftX;
	}

	@Override
	public double viewportTopLeftY() {
		return viewportTopLeftY;
	}

	@Override
	public double viewportWidth() {
		return viewportWidth;
	}

	@Override
	public double viewportHeight() {
		return viewportHeight;
	}

	@Override
	public double worldWidth() {
		return worldWidth;
	}

	@Override
	public double worldHeight() {
		return worldHeight;
	}

	@Override
	public double canvasWidth() {
		return getCanvas().getWidth();
	}

	@Override
	public double canvasHeight() {
		return getCanvas().getHeight();
	}

	@Override
	public void drawOnWorld(DrawingLambdaFx lambda) {
		lambda.draw(getGc());
	}

	@Override
	public void drawOnViewport(DrawingLambdaFx lambda) {
		lambda.draw(getGc());
	}

	@Override
	public void drawOnCanvas(DrawingLambdaFx lambda) {
        getGc().save();

        getGc().beginPath();

        getGc().rect(0, 
                     0, 
                     canvasWidth(), 
                     canvasHeight());
        
        getGc().clip();
        
		lambda.draw(getGc());

        getGc().restore();
	}

	public void addMouseEventFilter(EventType<MouseEvent> eventType, MouseEventHandlerFx handler) {
        addEventFilter(eventType, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                handleMouseEvent(event, handler);
            }
        });
	}

    private void handleMouseEvent(MouseEvent event, MouseEventHandlerFx handler) {
        double x = event.getX();
        double y = event.getY();
        
        double mxx = getGc().getTransform().getMxx();
        double myy = getGc().getTransform().getMyy();
        double tx = getGc().getTransform().getTx();
        double ty = getGc().getTransform().getTy();
        
        double worldX = (x - tx) / mxx;
        double worldY = (y - ty) / myy;
        
        World2dMouseEventFx mouseEventFx = new World2dMouseEventFx(event, 
                                                                   this,
                                                                   worldX,
                                                                   worldY);
        
        handler.handle(mouseEventFx);
    }

	public void clearCanvas() {
		getGc().clearRect(0, 0, canvasWidth(), canvasHeight());
	}

	public void relocateResizeViewport(double newTopLeftX, double newTopLeftY, double newWidth, double newHeight) {
		// TODO Auto-generated method stub
		
	}

	public void relocateViewport(double d, double e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double widthOnScreen(double widthInWorld) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double heightOnScreen(double heightInWorld) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double widthInWorld(double widthOnScreen) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double heightInWorld(double heightOnScreen) {
		// TODO Auto-generated method stub
		return 0;
	}
	

}
