package ca.ntro.app.fx.controls;

public interface NeedsRedrawListener {

	void needsRedraw();

}
