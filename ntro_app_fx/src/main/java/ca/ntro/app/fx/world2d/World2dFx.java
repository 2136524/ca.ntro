package ca.ntro.app.fx.world2d;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ca.ntro.app.fx.controls.World2dDrawingContext;
import ca.ntro.app.fx.controls.World2dMouseEventFx;
import ca.ntro.app.models.ModelValue;

public abstract class World2dFx implements ModelValue {

	private double width = 640;
	private double height = 360;

	private List<Object2dFx<?>> objects = new ArrayList<>();

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public List<Object2dFx<?>> getObjects() {
		return objects;
	}

	public void setObjects(List<Object2dFx<?>> objects) {
		this.objects = objects;
	}
	
	public World2dFx() {
        initialize();
	}
	
	
	protected abstract void initialize();
	protected abstract void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent);
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void addObject2d(Object2dFx object2d) {
		object2d.setWorld2d(this);
		objects.add(object2d);
		object2d.initialize();
	}

	public void draw(World2dDrawingContext context) {
		for(Object2dFx<?> objet2d: objects) {
			objet2d.draw(context);
		}
	}

	public void onTimePasses(double secondsElapsed) {
		for(Object2dFx<?> objet2d: objects) {
			objet2d.onTimePasses(secondsElapsed);
		}
	}

	public void copyDataFrom(World2dFx world2d) {
		for(int i = 0; i < objects.size(); i++) {
			world2d.copyDataTo(i, objects.get(i));
		}
	}

	protected void copyDataTo(int i, Object2dFx<?> object2d) {
		if(i < objects.size()) {
			object2d.copyDataFrom(objects.get(i));
		}
	}

	public void dispatchMouseEvent(World2dMouseEventFx world2dMouseEventFx) {
        double worldX = world2dMouseEventFx.worldX();
        double worldY = world2dMouseEventFx.worldY();

        boolean consumed = false;
        for(Object2dFx<?> object : getObjects()) {
            if(object.collidesWith(worldX-2, worldY-2, 4, 4)) {
                consumed = consumed || object.onMouseEvent(world2dMouseEventFx);
            }
        }

        if(!consumed) {
            onMouseEventNotConsumed(world2dMouseEventFx);
        }
	}

	public void removeObject2dIn(Set<String> toRemove) {
		
	}

	public void removeObject2dNotIn(Set<String> toRemove) {
		
	}

	public Object2dFx<?> objectById(String markerId) {
		// TODO Auto-generated method stub
		return null;
	}


}
