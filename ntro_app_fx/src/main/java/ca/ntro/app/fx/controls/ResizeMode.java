package ca.ntro.app.fx.controls;

public enum ResizeMode {
	
	RESIZE_VIEWPORT,
	SCALE_DRAWING;

}
