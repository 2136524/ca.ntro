package ca.ntro.app.fx.controls;

public interface World2dDrawingContext {
	
	double viewportTopLeftX();
	double viewportTopLeftY();
	double viewportWidth();
	double viewportHeight();

	double worldWidth();
	double worldHeight();

	double canvasWidth();
	double canvasHeight();
	
	void drawOnWorld(DrawingLambdaFx lambda);
	void drawOnViewport(DrawingLambdaFx lambda);
	void drawOnCanvas(DrawingLambdaFx lambda);

	double widthOnScreen(double widthInWorld);
	double heightOnScreen(double heightInWorld);
	double widthInWorld(double widthOnScreen);
	double heightInWorld(double heightOnScreen);
}
