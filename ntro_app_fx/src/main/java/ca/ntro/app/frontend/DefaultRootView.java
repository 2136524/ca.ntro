/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.frontend;

import ca.ntro.core.initialization.Ntro;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class DefaultRootView extends StackPane {
	
	public DefaultRootView() {
		super();
		
		HBox hbox = new HBox();
		
		String message = "No RootView. Please install a RootView";
		Label label = new Label("[INFO] " + message);
		
		hbox.getChildren().add(label);

		hbox.setAlignment(Pos.CENTER);
		getChildren().add(hbox);
		
		//hbox.setBackground(new Background(new BackgroundFill(Color.BLACK, new CornerRadii(0.8), new Insets(2))));
		
	    Timeline animation = new Timeline();

	    Duration time01 = new Duration(0);
	    Duration time02 = new Duration(500);
	    Duration time02b = new Duration(800);
	    Duration time03 = new Duration(1000);

	    KeyValue opacity01 = new KeyValue(label.opacityProperty(), 0.99);
	    KeyValue opacity02 = new KeyValue(label.opacityProperty(), 0.8);

	    KeyValue scaleX01 = new KeyValue(label.scaleXProperty(), 1.0);
	    KeyValue scaleX02 = new KeyValue(label.scaleXProperty(), 1.5);

	    KeyValue scaleY01 = new KeyValue(label.scaleYProperty(), 1.0);
	    KeyValue scaleY02 = new KeyValue(label.scaleYProperty(), 1.5);

	    KeyValue rotate01 = new KeyValue(label.rotateProperty(), 0);
	    KeyValue rotate02 = new KeyValue(label.rotateProperty(), 360);

	    animation.getKeyFrames().add(new KeyFrame(time01, opacity01));
	    animation.getKeyFrames().add(new KeyFrame(time02, opacity02));
	    animation.getKeyFrames().add(new KeyFrame(time03, opacity01));

	    animation.getKeyFrames().add(new KeyFrame(time01, scaleX01));
	    animation.getKeyFrames().add(new KeyFrame(time02, scaleX02));
	    animation.getKeyFrames().add(new KeyFrame(time03, scaleX01));

	    animation.getKeyFrames().add(new KeyFrame(time01, scaleY01));
	    animation.getKeyFrames().add(new KeyFrame(time02, scaleY02));
	    animation.getKeyFrames().add(new KeyFrame(time03, scaleY01));

	    animation.getKeyFrames().add(new KeyFrame(time02b, rotate01));
	    animation.getKeyFrames().add(new KeyFrame(time03, rotate02));

	    animation.setCycleCount(-1);
	    
	    Ntro.time().runAfterDelay(5000, () -> {
			animation.playFromStart();
	    });
	}

}
