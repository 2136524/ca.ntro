package services;

import ca.ntro.core.services.LoggerNtro;
import javafx.fxml.LoadException;

public class LoggerFx extends LoggerNtro {

	@Override
	public void fatal(Throwable t) {
		if(t instanceof LoadException) {

			fatalLoadException((LoadException) t);

		}else {

			super.fatal(t);
		}
	}

	private void fatalLoadException(LoadException e) {
		String message = "";

		if(e.getMessage() != null) {

			try {
				// XXX: must also support
				//      Windows paths, e.g.
				//     /C:/Users/mbergeron/4f5_mathieu_bergeron/pong/bin/main/racine.xml:6
				
				String filepathAndLineNumber = e.getMessage().trim();
				String[] segments = filepathAndLineNumber.split(":");
				String filepath = segments[segments.length - 2];
				int lineNumber = 0;
				if(segments.length > 1) {
					// XXX: LoadException might report the lineNumber
					//      for the last line of a tag, even if the error
					//      occurs when reading an attribute
					lineNumber = Integer.parseInt(segments[segments.length - 1]);
				}
				
				String filename = filepath;
				if(filepath.contains("/")) {
					segments = filepath.split("/");
				}else {
					segments = filepath.split("\\");
				}

				if(segments.length > 0) {
					filename = segments[segments.length - 1];
				}
				
				if(lineNumber == 0) {

					message += "(" + filename + ")";

				}else {
					
					message += "(" + filename + ":" + lineNumber + ")";
					
				}

			}catch(Throwable t) {
				message = e.getMessage();
			}
		}

		if(e.getCause() != null) {
			
			Throwable cause = e.getCause();
			
			if(cause instanceof ClassNotFoundException) {
				
				message += " class not found: " + cause.getMessage();
				
			}else {
				
				message += " " + cause.getClass().getSimpleName() + " " + cause.getMessage();

			}
		}

		fatal(message, e);

	}

}
